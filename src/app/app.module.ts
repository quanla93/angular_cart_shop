import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import {CartHeaderComponet} from "./cart-header/cart-header.componet";
import {ProductListComponent} from "./product-list/product-list.component";
import {CartSummaryComponent} from "./cart-summary/cart-summary.component";
import {PromoCodeComponent} from "./promo-code/promo-code.component";
import {FormsModule} from "@angular/forms";

@NgModule({
  declarations: [
    AppComponent,
    CartHeaderComponet,
    ProductListComponent,
    CartSummaryComponent,
    PromoCodeComponent
  ],
  imports: [
    BrowserModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
