import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-cart-summary',
  templateUrl: './cart-summary.component.html'
})
export class CartSummaryComponent implements OnInit {

  @Input() totalsum: number;
  @Input() discount: number;
  @Input() tax: number;

  constructor() { }

  ngOnInit() {
  }

}
